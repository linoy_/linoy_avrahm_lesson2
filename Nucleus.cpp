#include "Nucleus.h"

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	Gene _new_nucleus;
	_new_nucleus._start = start;
	_new_nucleus._end = end;
	_new_nucleus._on_complementary_dna_strand = on_complementary_dna_strand;
}

int Gene::get_start()const
{
	return _start;
}

int Gene::get_end()const
{
	return _end;
}

bool Gene::is_on_complementary_dna_strand()const
{
	return _on_complementary_dna_strand;
}


void Gene::set_start(const unsigned int start)
{
	_start = start;
}

void Gene::set_end(const unsigned int end)
{
	_end = end;
}

void Gene::set_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	_on_complementary_dna_strand = on_complementary_dna_strand;
}

void DNA::init(const std::string dna_sequence)
{
	_DNA_strand = dna_sequence;

	std::string complementary_dna_strand = "";
	for (unsigned int i = 0; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] == 'A')
		{
			complementary_dna_strand += 'T';
		}
		else if (dna_sequence[i] == 'G')
		{
			complementary_dna_strand += 'C';
		}
		else if (dna_sequence[i] == 'C')
		{
			complementary_dna_strand += 'G';
		}
		else if (dna_sequence[i] == 'T')
		{
			complementary_dna_strand += 'A';
		}
		else
		{
			std::cerr << "dna sequence is not valid" << std::endl;
			_exit(1); // exits with error code
		}
	}
	_complementary_DNA_stran = complementary_dna_strand;
}

std::string DNA::get_RNA_transcript(const Gene& gene) const
{
	bool _find_nucles = gene.is_on_complementary_dna_strand();
	std::string _the_nucleus = "";
	std::string ans = "";
	int i = 0;

	if (_find_nucles == true)
	{
		_the_nucleus = _DNA_strand;
	}
	else
	{
		_the_nucleus = _complementary_DNA_stran;
	}

	for (i = gene.get_start; i <= gene.get_end; i++)
	{
		if (_the_nucleus[i] == 'T' || _the_nucleus[i] == 't')
		{
			ans = ans + 'U';
		}
		else
		{
			ans = ans + _the_nucleus[i];
		}
	}

	return ans;
}

std::string DNA::get_reversed_DNA_strand() const
{
	std::string _the_nucleus = _DNA_strand;

	int n = _the_nucleus.length();

	// Swap character starting from two 
	// corners 
	for (int i = 0; i < n / 2; i++)
		std::swap(_the_nucleus[i], _the_nucleus[n - i - 1]);
}

unsigned int DNA::get_num_of_codon_appearances(const std::string& codon) const
{
	std::string _the_nucleus = _DNA_strand;

	int len = _the_nucleus.length();
	int count = 0;
	for (int i = 0; i < len; i++)
	{
		std::size_t found = _the_nucleus.find(codon);
		if (found != std::string::npos)
		{
			count++;
			i++;//i = the index is at the beginning of the codon and we need to + 2 but the loop +1 so 2-1=1
		}
		
	}

	return count;
}